package com.name.business.Threads;

import com.google.gson.JsonObject;
import com.name.business.businesses.FCMBusiness;
import com.name.business.entities.FCM;
import com.name.business.services.handle.FCMHelper;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import java.util.List;
import java.util.TimerTask;

/**
 * Created by luis on 8/04/17.
 */
public class FCMThread extends TimerTask {
    private FCMBusiness fcmBusiness;

    public FCMThread(FCMBusiness fcmBusiness) {
        super();
        this.fcmBusiness = fcmBusiness;
    }



    @Override
    public boolean cancel() {
        return super.cancel();
    }

    @Override
    public long scheduledExecutionTime() {
        return super.scheduledExecutionTime();
    }

    @Override
    public void run() {
        Either<IException, List<FCM>> fcm = fcmBusiness.getFCM(null, null, null, null);
        if (fcm.isRight()){
            beginFCM(fcm.right().value());


        }


    }

    private void beginFCM(List<FCM> value) {
        for (FCM fcm: value ) {
            System.out.println("******************************+++++++++++++");
            System.out.println("Promociones "+fcm.getTitle());
            System.out.println("Estado "+fcm.getStatus());
            sentFCMDevise(fcm);
            fcm.setStatus(0);
            fcmBusiness.updateFCM(fcm);
        }
    }

    private void sentFCMDevise(FCM fcmModel) {
        FCMHelper fcm = FCMHelper.getInstance();
        JsonObject dataObject = new JsonObject();
        dataObject.addProperty("titulo", "Este es el titular"); // See GSON-Reference
        dataObject.addProperty("descripcion", "Aquí estará todo el contenido de la noticia");// See GSON-Reference


        JsonObject notificationObject = new JsonObject();
        notificationObject.addProperty("title", "Notificacion "); // See GSON-Reference
        notificationObject.addProperty("body", "Enviado desde el Servidor Dropwizard!"); // See GSON-Reference


            try {
                    fcm.sendData(FCMHelper.TYPE_TO,"AAAAab3MCDY:APA91bHvj-DX1sCjzNhkcmuk-ffyf-U2qlTnBTJ950gararjRZfKT4mLwiA4sLzwxYQOOX7UX_QZ9TchZ1sB-saxaiqVBJK9eQ8kjKTzFD8kgl8oRv8KDZDWyDlHKSa6BShbwAOIRPEV",dataObject);


                  // fcm.sendTopicNotificationAndData("",notificationObject, dataObject);






                //fcm.sendData(FCMHelper.TYPE_TO, DEVICE_TEST, dataObject); // RECIPIENT is the token of the device, or device group, or a topic.
            } catch (Exception e) {
                e.printStackTrace();

            }


    }
}
