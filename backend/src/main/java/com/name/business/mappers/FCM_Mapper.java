package com.name.business.mappers;

import com.name.business.entities.FCM;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 9/04/17.
 */
public class FCM_Mapper implements ResultSetMapper<FCM> {
    @Override
    public FCM map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new FCM(
                resultSet.getLong("id"),
                resultSet.getString("title"),
                resultSet.getString("description"),
                resultSet.getString("data_info"),
                resultSet.getInt("status"),
                resultSet.getTimestamp("date_sent"),
                resultSet.getString("type")
        );
    }
}
